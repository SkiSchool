package org.cvut.skischool.core;

/**
 *
 * @author matej
 */
public class NamingConstants {

    public static final String LESSON_INDIVIDUAL = "individual";
    public static final String LESSON_GROUP = "group";
    public static final String LESSON_KINDERGARTEN = "kindergarten";
    
    public static final String ROLE_INSTRUCTOR = "instructor";
    public static final String ROLE_ADMINISTRATOR = "administrator";
}
