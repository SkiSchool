package org.cvut.skischool.core;

/**
 *
 * @author matej
 */
public class NumberConstants {

    public static final int HOUR_WIDTH = 60;
    public static final int BAR_MARGIN = 2;

    public static final String COLOR_AVAILABILITY = "lightgreen";
    public static final String COLOR_LESSON_INDIVIDUAL = "#FFA500";
    public static final String COLOR_LESSON_KINDERGARTEN = "#FF4500";

    public static final int DAY_START_HOUR = 8;
    public static final int DAY_START_MINUTES = 0;
    public static final int DAY_END_HOUR = 21;
    public static final int DAY_END_MINUTES = 0;

    public static final int GROUP_SIZE = 3;
}
